import React from "react"
import { graphql } from "gatsby"
import {PrismicLink, PrismicRichText} from "@prismicio/react";
import {faFacebook, faInstagram} from "@fortawesome/free-brands-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEarthEurope} from "@fortawesome/free-solid-svg-icons";
import Layout from "../components/Layout";
import {GatsbyImage} from "gatsby-plugin-image";

const Exhibitors = ({ data }) => {
  if (!data) return null
  const exhibitors= data.prismicExhibitors

  console.log(exhibitors)

  return (
    <Layout>
      <h2 className="container mx-auto px-10 my-10 text-4xl font-bold">{exhibitors.data.name.text}</h2>
      <div className="container mx-auto px-10 py-10 flex gap-x-10">
        <div className="w-2/3">
          <ul className="mb-10">
            {exhibitors.data.appellations.map(appellation => {
              return <li>{appellation.appellation.document.data.name.text}</li>
            })}
          </ul>
          <PrismicRichText field={exhibitors.data.description.richText} />
          <ul className="mt-10 flex gap-4">
            {exhibitors.data.website &&
              <li><PrismicLink field={exhibitors.data.website} className="transition py-3 px-10 text-center bg-slate-800 text-white block rounded">
                <FontAwesomeIcon icon={faEarthEurope} fixedWidth className="mb-2 text-3xl" />
                <div className="font-bold">Website</div>
              </PrismicLink></li>
            }
            {exhibitors.data.facebook &&
              <li><PrismicLink field={exhibitors.data.facebook} className="transition py-3 px-10 text-center bg-facebook text-white block rounded">
                <FontAwesomeIcon icon={faFacebook} fixedWidth className="mb-2 text-3xl" />
                <div className="font-bold">Facebook</div>
              </PrismicLink></li>
            }
            {exhibitors.data.instagram &&
              <li><PrismicLink field={exhibitors.data.instagram} className="transition py-3 px-10 text-center bg-instagram text-white block rounded">
                <FontAwesomeIcon icon={faInstagram} fixedWidth className="mb-2 text-3xl" />
                <div className="font-bold">Instagram</div>
              </PrismicLink></li>
            }
          </ul>
        </div>
        <div className="w-1/3">
          <GatsbyImage
            alt={exhibitors.data.cover.alt}
            className="rounded max-h-full"
            image={exhibitors.data.cover.gatsbyImageData}
          />
        </div>
      </div>
    </Layout>
  )
}

export default Exhibitors

export const pageQuery = graphql`
  query ExhibitorsBySlug($uid: String!) {
    prismicExhibitors(uid: { eq: $uid }) {
      id
      data {
        name {
          text
        }
        description {
          richText
        }
        website {
          url
          target
        }
        facebook {
          url
          target
        }
        instagram {
          url
          target
        }
        cover {
          alt
          gatsbyImageData
        }
        appellations {
          appellation {
            id
            document {
              ... on PrismicAppellation {
                id
                data {
                  name {
                    text
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`