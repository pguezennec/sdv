import React from "react"
import { graphql } from "gatsby"

const Editions = ({ data }) => {
  if (!data) return null
  const editions= data.prismicEditions

  return (
    <>
      <h1>{editions.data.title.text}</h1>
      <div dangerouslySetInnerHTML={{ __html: editions.data.date_headline.text }} />
    </>
  )
}

export default Editions

export const pageQuery = graphql`
  query EditionsBySlug($id: String!) {
    prismicEditions(id: { eq: $id }) {
      id
      data {
        title {
          text
        }
        date_headline {
          text
        }
      }
    }
  }
`