import {graphql, useStaticQuery} from "gatsby";

export const useCurrentEdition = () => {
  const { allPrismicEditions } = useStaticQuery(
    graphql`
      query IndexQuery {
        allPrismicEditions(sort: {data: {start_date: DESC}}, limit: 1) {
          edges {
            node {
              data {
                title {
                  text
                  html
                  richText
                }
                date_headline {
                  text
                  html
                  richText
                }
                address {
                  richText
                }
                phone_number
                coordinates {
                  latitude
                  longitude
                }
                exhibitors {
                  exhibitor {
                    slug
                    document {
                      ... on PrismicExhibitors {
                        id
                        uid
                        data {
                          name {
                            text
                          }
                          description {
                            richText
                          }
                          website {
                            id
                            url
                            target
                          }
                          facebook {
                            id
                            url
                            target
                          }
                          instagram {
                            id
                            url
                            target
                          }
                          cover {
                            alt
                            gatsbyImageData
                          }
                          appellations {
                            appellation {
                              id
                              document {
                                ... on PrismicAppellation {
                                  id
                                  data {
                                    name {
                                      text
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
                body {
                  ... on PrismicEditionsDataBodyImage {
                    id
                    slice_type
                    primary {
                      image {
                        url
                        alt
                        gatsbyImageData(placeholder: BLURRED)
                      }
                    }
                  }
                  ... on PrismicEditionsDataBodyText {
                    id
                    slice_type
                    primary {
                      text {
                        richText
                      }
                    }
                  }
                  ... on PrismicEditionsDataBodyTextAndImage {
                    id
                    slice_type
                    primary {
                      text {
                        richText
                        html
                        richText
                        text
                      }
                      image {
                        url
                        alt
                        gatsbyImageData(placeholder: BLURRED)
                      }
                      image_alignment
                    }
                  }
                  ... on PrismicEditionsDataBodyHero {
                    id
                    slice_type
                    primary {
                      cta_text
                      cta_link
                      heading {
                        text
                      }
                      image {
                        alt
                        url
                        gatsbyImageData
                      }
                      subheading {
                        text
                      }
                    }
                    items {
                      text {
                        richText
                      }
                    }
                  }
                  ... on PrismicEditionsDataBodyQuote {
                    id
                    slice_type
                    primary {
                      author
                      avatar {
                        gatsbyImageData
                        url
                        alt
                      }
                      role
                      text {
                        richText
                      }
                    }
                  }
                }
                body1 {
                  ... on PrismicEditionsDataBody1CateringBlock {
                    id
                    primary {
                      image {
                        gatsbyImageData
                        alt
                      }
                      lead {
                        richText
                      }
                      text {
                        richText
                      }
                      title1 {
                        text
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `
  )

  return allPrismicEditions.edges[0].node.data
}