import * as React from "react"
import { Link, HeadFC, PageProps } from "gatsby"

const NotFoundPage: React.FC<PageProps> = () => {
  return (
    <main>
      <h1>Page introuvable</h1>
      <p>
        Désolé 😔, nous n'avons pas trouvé la page que vous recherchez.
      </p>
        {process.env.NODE_ENV === "development" ? (
          <p>
            Vous pouvez créer une page dans <code>src/pages/</code>.
          </p>
        ) : null}
      <p>
        <Link to="/">Aller à la page l'accueil</Link>.
      </p>
    </main>
  )
}

export default NotFoundPage

export const Head: HeadFC = () => <title>Pas introuvable</title>
