import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import {PrismicRichText} from "@prismicio/react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faLocationDot, faPhone} from "@fortawesome/free-solid-svg-icons";
import {StaticImage} from "gatsby-plugin-image";

const PracticalInfo = ({ data }) => {
  const edition = useCurrentEdition()

  console.log(edition);

  const hasMap = edition.coordinates.latitude && edition.coordinates.longitude;

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        <div className="grid grid-cols-3 gap-10 mb-10 items-center">
          <div className="flex flex-col justify-center h-full bg-rose-800 p-10 rounded text-center text-white">
            <p className="text-xl font-bold pb-4">Salon des Grand Vins et Produits du Terroir de Tramolé</p>
            <PrismicRichText field={edition.title.richText} />
            <PrismicRichText field={edition.date_headline.richText} />
          </div>
          <div className="flex flex-col justify-center h-full bg-slate-100 p-10 rounded">
            <div class="flex">
              <FontAwesomeIcon icon={faLocationDot} fixedWidth className="py-1 pr-2" />
              <div><PrismicRichText field={edition.address.richText} /></div>
            </div>
            <div class="flex pt-4">
              <FontAwesomeIcon icon={faPhone} fixedWidth className="py-1 pr-2" />
              <p><a href={`tel:${edition.phone_number}`} className="hover:underline">{edition.phone_number}</a></p>
            </div>
          </div>
            <StaticImage
              src="../images/salle-des-fetes-tramole.jpg"
              alt="Salle des Fêtes de Tramolé"
              className="h-full rounded"
            />
        </div>
        { hasMap &&
          <MapContainer style={{ height: '400px' }} center={[edition.coordinates.latitude, edition.coordinates.longitude]} zoom={9} scrollWheelZoom={false}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={[edition.coordinates.latitude, edition.coordinates.longitude]}>
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
              </Popup>
            </Marker>
          </MapContainer>
        }
      </div>
    </Layout>)
}

export default PracticalInfo

export const Head: HeadFC = () => <title>Infos pratiques du Salon des Vins de Tramolé</title>
