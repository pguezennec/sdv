import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";

const Partners = () => {
  const edition = useCurrentEdition()

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        Espace Partenaires
      </div>
    </Layout>)
}

export default Partners

export const Head: HeadFC = () => <title>Partenaires du Salon des Vins de Tramolé</title>
