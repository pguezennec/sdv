import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";

const Press = () => {
  const edition = useCurrentEdition()

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        Espace Presse
      </div>
    </Layout>)
}

export default Press

export const Head: HeadFC = () => <title>Espace Presse du Salon des Vins de Tramolé</title>
