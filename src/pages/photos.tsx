import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";

const Photos = () => {
  const edition = useCurrentEdition()

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        Espace Photos
      </div>
    </Layout>)
}

export default Photos

export const Head: HeadFC = () => <title>Photos du Salon des Vins de Tramolé</title>
