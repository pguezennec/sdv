import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";

const Contact = () => {
  const edition = useCurrentEdition()

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        Page de contact
      </div>
    </Layout>)
}

export default Contact

export const Head: HeadFC = () => <title>Contacter le Salon des Vins de Tramolé</title>
