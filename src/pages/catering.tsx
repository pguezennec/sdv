import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";
import {GatsbyImage} from "gatsby-plugin-image";
import {PrismicRichText} from "@prismicio/react";

const Catering = () => {
  const edition = useCurrentEdition()

  console.log(edition)

  return (<Layout>
    <h2 className="container mx-auto px-10 my-10 text-4xl font-bold">Restauration</h2>
    <div className="container mx-auto py-10 px-10 grid grid-cols-4 gap-4 rounded content-center">
      {edition.body1.map(slice => {
        return <div key={slice.id} className="shadow-lg">
          <GatsbyImage
            alt={slice.primary.image.alt}
            image={slice.primary.image.gatsbyImageData}
            width="500"
            className="rounded-t h-40"
          />
          <h3 className="font-bold text-xl text-slate-700 p-4">
            {slice.primary.title1.text}
          </h3>
          <div className="font-bold text-rose-800 px-4 mb-4">
            <PrismicRichText field={slice.primary.lead.richText} />
          </div>
          <div className="px-4 mb-4">
            <PrismicRichText field={slice.primary.text.richText} />
          </div>
        </div>
      })}
    </div>
  </Layout>)
}

export default Catering

export const Head: HeadFC = () => <title>Restauration au Salon des Vins de Tramolé</title>
