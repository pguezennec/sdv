import * as React from "react"
import type { HeadFC } from "gatsby"
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebook, faInstagram} from "@fortawesome/free-brands-svg-icons";
import {faEarthEurope} from "@fortawesome/free-solid-svg-icons";
import {GatsbyImage} from "gatsby-plugin-image";

const Exhibitors = () => {
  const edition = useCurrentEdition()

  return (<Layout>
      <div className="container mx-auto py-10 px-10">
        <div className="grid grid-cols-3 gap-4 rounded">
        {edition.exhibitors.map(exhibitor => {
          const doc = exhibitor.exhibitor.document;
          const linkIconClassNames = "transition m-4 py-1 px-2 pr-3 rounded-full bg-slate-200 text-sm hover:bg-slate-300";
          return <div key={doc.id} className="shadow-lg">
            <a href={'/exhibitors/' + doc.uid} className="block bg-rose-800 text-white border-b-1 border-white rounded-t">
              <GatsbyImage
                alt={doc.data.cover.alt}
                className="rounded-t"
                image={doc.data.cover.gatsbyImageData}
              />
            </a>
            <h3 className="font-bold text-xl text-slate-700 p-4">{doc.data.name.text}</h3>
            <div className="font-bold text-rose-800 px-4 mb-4">Stand X</div>
            <div className="px-4 mb-4">
              <ul>
                {doc.data.appellations.map(appellation => {
                  return <li>{appellation.appellation.document.data.name.text}</li>
                })}
              </ul>
            </div>
            <div className="flex">
              {doc.data.website && <a href={doc.data.website.url} target="_blank" className={linkIconClassNames}><FontAwesomeIcon icon={faEarthEurope} className="mr-2" />Site web</a>}
              {doc.data.facebook && <a href={doc.data.facebook.url} target="_blank" className={linkIconClassNames}><FontAwesomeIcon icon={faFacebook} className="mr-2" />Facebook</a>}
              {doc.data.instagram && <a href={doc.data.instagram.url} target="_blank" className={linkIconClassNames}><FontAwesomeIcon icon={faInstagram} className="mr-2" />Instagram</a>}
            </div>
          </div>
        })}
        </div>
      </div>
    </Layout>)
}

export default Exhibitors

export const Head: HeadFC = () => <title>Exposants au Salon des Vins de Tramolé</title>
