import * as React from "react"
import type { HeadFC } from "gatsby"
import TextSlice from "../components/TextSlice";
import TextAndImageSlice from "../components/TextAndImageSlice";
import HeroSlice from "../components/HeroSlice";
import QuoteSlice from "../components/QuoteSlice";
import Layout from "../components/Layout";
import {useCurrentEdition} from "../hooks/currentEdition";

const Index = () => {
  const edition = useCurrentEdition()

  return (<Layout>
    {edition.body.map(slice => {
      if (slice.slice_type === 'text') {
        return <TextSlice key={slice.id} slice={slice} />
      }
      else if (slice.slice_type === 'text_and_image') {
        return <TextAndImageSlice key={slice.id} slice={slice}/>
      }
      else if (slice.slice_type === 'hero') {
        return <HeroSlice key={slice.id} slice={slice}/>
      }
      else if (slice.slice_type === 'quote') {
        return <QuoteSlice key={slice.id} slice={slice}/>
      }
    })}
  </Layout>)
}

export default Index

export const Head: HeadFC = () => <title>Salon des Vins de Tramolé</title>
