import * as React from "react"
import MainMenu from "./MainMenu";
import Logo from "../assets/svg/logo.svg";

const Header = () => {

  return <header className="text-white bg-gradient-to-b from-rose-800 to-rose-950">
    <div className="container mx-auto px-10 flex flex-col items-center">
      <MainMenu />
      <h1 className="mt-8 mb-16 font-bold text-rose-200">
        <Logo className="w-64" />
      </h1>
    </div>
  </header>
}

export default Header
