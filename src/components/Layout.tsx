import * as React from "react"
import Header from "./Header";
import Footer from "./Footer";

const Layout = ({ children }) => {

  return <div className="flex flex-col h-screen justify-between">
    <Header />
    <main className="mb-auto text-rose-950">
      {children}
    </main>
    <Footer />
  </div>
}

export default Layout