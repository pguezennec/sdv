import * as React from "react"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPaperPlane, faPhone} from "@fortawesome/free-solid-svg-icons";
import {faFacebook} from "@fortawesome/free-brands-svg-icons";
import {Link} from "gatsby";

const Footer = () => {

  const linkClassNames = "transition-all hover:underline hover:text-rose-200";

  return <footer className="mt-10 bg-gradient-to-tr from-rose-950 to-rose-900 text-rose-100">
    <div className="container p-10 mx-auto grid grid-cols-4 divide-x divide-rose-300">
      <div className="px-4">
        Logo
      </div>
      <div className="px-4">
        <h3 className="pb-4 text-lg font-bold">Le salon</h3>
        <ul>
          <li><Link to="/" className={linkClassNames}>Édition 2023</Link></li>
          <li><Link to="/exhibitors" className={linkClassNames}>Les exposants</Link></li>
          <li><Link to="/catering" className={linkClassNames}>Restauration</Link></li>
          <li><Link to="/practical-info" className={linkClassNames}>Infos pratiques</Link></li>
        </ul>
      </div>
      <div className="px-4">
        <h3 className="pb-4 text-lg font-bold">Autour du salon</h3>
        <ul>
          <li><a href="https://www.afltramole.fr" className={linkClassNames} target="_blank">AFL Tramolé</a></li>
          <li><Link to="/press" className={linkClassNames}>Presse</Link></li>
          <li><Link to="/photos" className={linkClassNames}>Photos</Link></li>
          <li><Link to="/partners" className={linkClassNames}>Partenaires</Link></li>
        </ul>
      </div>
      <div className="px-4">
        <h3 className="pb-4 text-lg font-bold">Contact</h3>
        <ul>
          <li><a href="tel:+33632816044" className={linkClassNames}><FontAwesomeIcon icon={faPhone} fixedWidth className="mr-3" />Téléphone</a></li>
          <li><Link to="/contact" className={linkClassNames}><FontAwesomeIcon icon={faPaperPlane} fixedWidth className="mr-3" />Formulaire de contact</Link></li>
          <li><a href="https://www.facebook.com/SalonDesVinsTramole" target="_blank" className={linkClassNames}><FontAwesomeIcon icon={faFacebook} fixedWidth className="mr-3" />Facebook</a></li>
        </ul>
      </div>
    </div>
    <div className="container p-10 mx-auto text-center text-rose-300 italic">
      L’abus d’alcool est dangereux pour la santé - À consommer avec modération<br />
      La vente de boissons alcoolisées est interdite aux mineurs de moins de 18 ans (code de la santé publique Art L.3342-1 ET L.3353-3)
    </div>
  </footer>
}

export default Footer
