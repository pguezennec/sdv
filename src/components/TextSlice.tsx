import * as React from "react"
import {PrismicRichText} from "@prismicio/react";

const TextSlice = ({slice}) => {
  return <div className="container mx-auto py-10 px-10">
    <PrismicRichText field={slice.primary.text.richText} />
  </div>
}

export default TextSlice