import * as React from "react"
import {PrismicRichText} from "@prismicio/react";
import {GatsbyImage} from "gatsby-plugin-image";

const TextAndImageSlice = ({slice}) => {

  return <div className="flex gap-x-10 container mx-auto px-10 py-10">
    <div className={`w-1/3 xl:w-1/4 ${slice.primary.image_alignment === 'Right' ? " order-last" : ""}`}>
      <GatsbyImage
        alt={slice.primary.image.alt}
        className="rounded max-h-full"
        image={slice.primary.image.gatsbyImageData}
      />
    </div>
    <div className="w-2/3 xl:w-3/4">
      <PrismicRichText field={slice.primary.text.richText} />
    </div>
  </div>
}

export default TextAndImageSlice