import * as React from "react"
import {PrismicRichText} from "@prismicio/react";
import {GatsbyImage} from "gatsby-plugin-image";

const HeroSlice = ({slice}) => {

  return <div className="flex flex-col md:flex-row gap-10 items-center container mx-auto px-10 py-10">
    <div className="xl:w-1/4 md:order-last">
      <GatsbyImage
        alt={slice.primary.image.alt}
        className="rounded-lg max-h-full after:absolute after:inset-0 after:border-8 after:border-amber-800/50 after:rounded-lg"
        image={slice.primary.image.gatsbyImageData}
      />
    </div>
    <div className="xl:w-3/4">
      <p className="font-display text-3xl text-amber-600">{slice.primary.subheading.text}</p>
      <p className="font-display text-5xl text-rose-900">{slice.primary.heading.text}</p>
      {slice.items.map(item =>
        <div className="pt-4"><PrismicRichText field={item.text.richText} /></div>
      )}
    </div>
  </div>
}

export default HeroSlice