import * as React from "react"
import {PrismicRichText} from "@prismicio/react";
import {GatsbyImage} from "gatsby-plugin-image";

const QuoteSlice = ({slice}) => {

  console.log(slice);

  return <div className="flex gap-10 container mx-auto px-10 py-10">
    <div className="w-1/4">
      <GatsbyImage
        alt={slice.primary.avatar.alt}
        className="rounded-full after:absolute after:inset-0 after:border-8 after:border-amber-800/50 after:rounded-full"
        image={slice.primary.avatar.gatsbyImageData}
      />
    </div>
    <div className="w-3/4">
      <PrismicRichText field={slice.primary.text.richText} />
      <p className="font-signature text-3xl">{slice.primary.author}</p>
      <p className="text-sm text-amber-500">{slice.primary.role}</p>
    </div>
  </div>
}

export default QuoteSlice