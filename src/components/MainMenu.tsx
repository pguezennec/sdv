import * as React from "react"
import {Link} from "gatsby";

const menuItems = [
  {
    to: '/',
    value: 'Accueil'
  },
  {
    to: '/exhibitors',
    value: 'Exposants'
  },
  {
    to: '/catering',
    value: 'Restauration'
  },
  {
    to: '/practical-info',
    value: 'Infos pratiques'
  },
]

const MainMenu = () => {
  return <nav aria-labelledby="mainmenulabel">
    <h3 id="mainmenulabel" className="sr-only">Main Menu</h3>
    <ul className="flex pt-4 whitespace-nowrap">
      {menuItems.map((item) => {
        return <li key={item.to}><Link to={item.to} activeClassName="after:bg-rose-600" className="
          block p-4 uppercase font-display drop-shadow-[0_1px_3px_rgba(0,0,0,0.5)] block transition-all
          after:absolute after:inset-x-6 after:bottom-4 after:h-0.5 after:bg-transparent after:drop-shadow-none after:transition-all
          hover:-translate-y-0.5
          hover:after:bottom-3 hover:after:bg-rose-400
        ">
          {item.value}
        </Link></li>
      })}
    </ul>
  </nav>
}

export default MainMenu
