const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const pages = await graphql(`
    {
      allPrismicEditions {
        nodes {
          id
        }
      }
      allPrismicExhibitors {
        nodes {
          uid
        }
      }
    }
  `)

  const editionTemplate = path.resolve("src/templates/editions.tsx")
  pages.data.allPrismicEditions.nodes.forEach(editions => {
    createPage({
      path: `/${editions.id}`,
      component: editionTemplate,
      context: {
        id: editions.id,
      },
    })
  })

  const exhibitorTemplate = path.resolve("src/templates/exhibitors.tsx")
  pages.data.allPrismicExhibitors.nodes.forEach(exhibitors => {
    createPage({
      path: `/exhibitors/${exhibitors.uid}`,
      component: exhibitorTemplate,
      context: {
        uid: exhibitors.uid,
      },
    })
  })

}