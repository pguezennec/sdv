/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    `./src/pages/**/*.{js,jsx,ts,tsx}`,
    `./src/components/**/*.{js,jsx,ts,tsx}`,
    `./src/templates/**/*.{js,jsx,ts,tsx}`,
  ],
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
    },
    extend: {
      fontFamily: {
        'sans': ['Roboto', ...defaultTheme.fontFamily.sans],
        'display': ['Spicy Sushi Regular', ...defaultTheme.fontFamily.sans],
        'signature': ['Hurricane', 'cursive', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        'facebook': '#3b5999',
        'messenger': '#0084ff',
        'twitter': '#55acee',
        'linkedin': '#0077b5',
        'skype': '#00aff0',
        'dropbox': '#007ee5',
        'wordpress': '#21759b',
        'vimeo': '#1ab7ea',
        'slideshare': '#0077b5',
        'vk': '#4c75a3',
        'tumblr': '#34465d',
        'yahoo': '#410093',
        'google-plus': '#dd4b39',
        'pinterest': '#bd081c',
        'youtube': '#cd201f',
        'stumbleupon': '#eb4924',
        'reddit': '#ff5700',
        'quora': '#b92b27',
        'yelp': '#af0606',
        'weibo': '#df2029',
        'producthunt': '#da552f',
        'hackernews': '#ff6600',
        'soundcloud': '#ff3300',
        'blogger': '#f57d00',
        'whatsapp': '#25d366',
        'wechat': '#09b83e',
        'line': '#00c300',
        'medium': '#02b875',
        'vine': '#00b489',
        'slack': '#3aaf85',
        'instagram': '#e4405f',
        'dribbble': '#ea4c89',
        'flickr': '#ff0084',
        'foursquare': '#f94877',
        'behance': '#131418',
        'snapchat': '#fffc00'
      },
    },
  },
  plugins: [],
}
